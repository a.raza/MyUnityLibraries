﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtagonistScript : MonoBehaviour
{
    #region Public Variables
     
    public static ProtagonistScript instance = null;
    [Range(10, 25)]
    public float m_speed = 10f;
    public Dimension m_dimension;

    #endregion

    #region Private Variables

    private Controller m_controller;

    #endregion

    #region Unity callbacks

    void Awake()
    {
        if (instance == null)
            instance = this;

        m_controller = new Controller(this.transform, m_speed, m_dimension);
    }

    void Update()
    {
        m_controller.OnUpdate();

        if (m_controller.Is_controllerActive())
        {
            m_controller.Locomote();    //Rotation
            m_controller.Move();        //Movement
        }
    }

    #endregion

}