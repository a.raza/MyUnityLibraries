﻿using UnityEngine;

public class Controller
{
    #region Private Variables

    private float m_xaxis, m_zaxis, m_speed;
    private Vector3 m_postion;
    private Transform m_mainTransform, m_cam;
    private Dimension m_dimension;
    private Vector3 m_targetDir, m_dir;
    private Rigidbody m_rigidbody;
    private Animator m_animator;

    #endregion


    #region Constructor

    public Controller(Transform a_mainTransform, float a_speed, Dimension a_dimension)
    {
        m_cam = Object.FindObjectOfType<Camera>().transform;

        m_mainTransform = a_mainTransform;

        m_speed = a_speed;

        m_dimension = a_dimension;

        m_rigidbody = FetchRigidbody();

        m_animator = FetchAnimator();
    }

    #endregion

    public void OnUpdate()
    {
        m_postion = Position();
    }

    private Vector3 Position()
    {
        m_xaxis = Input.GetAxis("Horizontal");
        m_zaxis = Input.GetAxis("Vertical");

        // if player is using the controls...
        if (Is_controlInUse().Equals(true))
        {
            if(m_dimension.Equals(Dimension.ThreeDimensional))
            {
                var a_forward = m_cam.TransformDirection(Vector3.forward);

                // Player is moving on ground, Y component of camera facing is not relevant.
                a_forward.y = 0.0f;
                a_forward = a_forward.normalized;

                // Calculate target direction based on camera forward and direction key.
                var a_right = new Vector3(a_forward.z, 0, -a_forward.x);

                var a_targetDirection = a_forward * m_zaxis + a_right * m_xaxis;

                return a_targetDirection;   
            }
            else
            {
                // the virtual position will move according to the player controls
                var a_targetDirection = new Vector2(m_xaxis, m_zaxis);

                return a_targetDirection;
            }
        }
        else
        {
            return Vector3.zero;
        }
    }

    public Vector3 GetPosition()
    {
        return m_postion;
    }

    protected bool Is_controlInUse()
    {
        return (!Input.GetAxis("Horizontal").Equals(0)
                ||
                !Input.GetAxis("Vertical").Equals(0)
                ||
                !Input.GetAxis("Horizontal").Equals(0)
                &&
                !Input.GetAxis("Vertical").Equals(0));
    }

    public bool Is_controllerActive()
    {
        return m_postion != Vector3.zero;
    }

    public void Move()
    {
        // This will move the character according to the controller
        m_mainTransform.position += new Vector3
            (
                GetPosition().x * Speed(), //updating horizontal movement
                GetPosition().y * Speed(), //updating Vertical movement for 2D
                GetPosition().z * Speed()  //updating Vertical movement for 3D
            );
    }

    public void Locomote()
    {
        //getting the distance between the virtual position(Whic is one step a head of the current position of the protagonist) and the current position of the protagonsit
        m_targetDir = VirtualPosition() - m_rigidbody.velocity;

        //only forward, backward, left and right direction are required here
        m_dir = new Vector3(m_targetDir.x, m_targetDir.y, m_targetDir.z);

        // This will rotate the character according to the controller
        m_mainTransform.rotation = Quaternion.Lerp
            (
                m_mainTransform.rotation,       // get the current rotaiont of the protagonist
                Quaternion.LookRotation(m_dir), // make protagonist look towards the direction
                Speed()                         // giving it a speed to make a smooth turn/rotation
            );
    }

    public Vector3 VirtualPosition()
    {
        return new Vector3
            (
                //getting the current x axis of the protagonist, and adding +1 to the x axis.. returns one step a head of the current position of the protagonist
                m_rigidbody.velocity.x + GetPosition().x
                ,
                //getting the current z axis of the protagonist, and adding +1 to the y axis.. returns one step a head of the current position of the protagonist
                m_rigidbody.velocity.y + GetPosition().y
                ,
                //getting the current z axis of the protagonist, and adding +1 to the z axis.. returns one step a head of the current position of the protagonist
                m_rigidbody.velocity.z + GetPosition().z
            );
    }

    private Rigidbody FetchRigidbody()
    {
        Rigidbody a_rigidbody;
        
        if (m_mainTransform.GetComponent<Rigidbody>().Equals(null))
        {
            m_mainTransform.gameObject.AddComponent<Rigidbody>();
            a_rigidbody =  m_mainTransform.GetComponent<Rigidbody>();
        }
        else
        {
            a_rigidbody = m_mainTransform.GetComponent<Rigidbody>();
        }

        a_rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;

        return a_rigidbody;
    }

    public Rigidbody GetRigidbody()
    {
        return m_rigidbody;
    }


    private Animator FetchAnimator()
    {
        Animator a_animator;
        
        if (m_mainTransform.GetComponent<Animator>().Equals(null))
        {
            m_mainTransform.gameObject.AddComponent<Animator>();
            a_animator = m_mainTransform.GetComponent<Animator>();
        }
        else
        {
            a_animator = m_mainTransform.GetComponent<Animator>();
        }

        return a_animator;
    }

    public Animator GetAnimator()
    {
        return m_animator;
    }

    private float Speed()
    {
        return m_speed * Time.deltaTime;
    }
}

public enum Dimension
{
    TwoDimensional,
    ThreeDimensional,
};